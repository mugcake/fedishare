export interface Button {
  id?: string;
  href?: string;
}

export interface Query {
  active?: boolean;
  currentWindow?: boolean;
  url?: string;
  title?: string;
}

export interface Instance {
  name: string;
  post: string;
  icon: string;
  host: string;
  button?: Button;
}

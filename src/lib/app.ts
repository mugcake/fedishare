import { renderItems } from './dom';

export class App {
  public render() {
      renderItems();
  }
}

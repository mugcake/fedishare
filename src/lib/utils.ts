import { Query, Instance } from './interface';
import * as instance from './instances';

// ---------- Commons

export function generateArray(obj: any): Instance[] {
  const item = [];

  Object.keys(obj).map((index) => item.push(obj[index]));
  return item;
}

// ----------------------- Main section -----------------------

export function getCurrentTabInfo() {
  const queryInfo: Query = {
    active: true,
    currentWindow: true,
  };

  chrome.tabs.query(queryInfo, (tabs: Query[] = []) => {
    if (tabs.length === 0) return;

    const tab = tabs[0] || {};
    makeButtons(tab);
  });
}

function urlAssigner(instance: Instance) {
  const { id, href } = instance.button;
  const element = <HTMLAnchorElement>document.getElementById(id);

  element.href = href;
}

function makeButtons(tabQuery: Query) {
  const socialBtns = generateArray(instance);

  // Getting the host of the instance
  socialBtns.map((item: Instance) => {
    const { name, host, post } = item;
    const element = <HTMLAnchorElement>document.getElementById(`url-${name.toLowerCase()}`);
    const itemHost: any = browser.storage.sync.get(`${name.toLowerCase()}Host`); // fix this
    // TODO improve regex
    const str = post.replace(/Title/, tabQuery.title).replace(/Url/, tabQuery.url);

    itemHost.then((res: string) => {
      element.href = [res[`${name.toLowerCase()}Host`] || host] + str;
    });
    item.button = { id: name, href: itemHost };
  });
  socialBtns.forEach(urlAssigner);
}

// Hide-Show items
export function enableItem(instance: Instance) {
  const name = instance.name.toLowerCase();

  chrome.storage.sync.get(`${name}Check`, (value) => {
    if (value[`${name}Check`]) {
      document.getElementById(`url-${name}`).removeAttribute('hidden');
    }
  });
}

// ----------------------- Options section -----------------------

export function saveItems(instance: Instance) {
  const name = instance.name.toLowerCase();
  const checkbox = <HTMLInputElement>document.getElementById(`${name}-check`);
  const container = <HTMLInputElement>document.getElementById(`${name}-host`);

  browser.storage.sync.set({
    [`${name}Host`]: container.value,
  });
  if (checkbox.checked) {
    browser.storage.sync.set({
      [`${name}Check`]: (checkbox.checked = true),
    });
  } else {
    browser.storage.sync.set({
      [`${name}Check`]: (checkbox.checked = false),
    });
  }
}

export function gettingItems(instance: Instance) {
  const name = instance.name.toLowerCase();
  const containerCheck = <HTMLInputElement>document.getElementById(`${name}-check`);
  const containerHost: any = document.getElementById(`${name}-host`); // fix this

  const gettingItem = browser.storage.sync.get(`${name}Host`);
  gettingItem.then((res) => {
    containerHost.value = res[`${name}Host`] || instance.host;
  });
  chrome.storage.sync.get(`${name}Check`, (value) => {
    if (value[`${name}Check`]) {
      containerCheck.checked = true;
    }
  });
}

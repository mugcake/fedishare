import { getCurrentTabInfo, enableItem, generateArray } from './utils';
import * as instance from './instances';

const items = generateArray(instance);

export function renderItems() {
  const containerItems = document.getElementById('root');

  containerItems.innerHTML = '';
  items.forEach((value) => {
    const name = value.name;
    const html = `
    <a class="button btn-${name.toLowerCase()}" id="url-${name.toLowerCase()}" hidden>
      <span><i class="fa fa-${value.icon} icon"></i></span>
      <span>${name}</span>
    </a>`;

    containerItems.insertAdjacentHTML('afterbegin', html);
    getCurrentTabInfo();
  });

  items.map((value) => enableItem(value));

  // Options button

  const htmlOptions = `
  <a class="button btn-options" id="options">
    <span><i class="fa fa-cog icon"></i></span>
    <span>Options</span>
  </a>`;

  containerItems.insertAdjacentHTML('beforeend', htmlOptions);
  // Open options page
  const optionsBtn = document.getElementById('options');

  function openOptions() {
    browser.runtime.openOptionsPage();
  }
  optionsBtn.addEventListener('click', openOptions);
}

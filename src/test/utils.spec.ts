import { generateArray } from '../lib/utils';
import * as instance from '../lib/instances';

describe('testing utils', () => {
  describe('testing generateArray', () => {
    it('should return an array of instances', () => {
      const actual = generateArray(instance);
      const testInstances = actual.slice(1, 3); // Friendica and GNUsocial
      const expected = [instance.friendica, instance.gnusocial];
      expect(testInstances).toEqual(expected);
    });
  });
  // TODO include the rest
});

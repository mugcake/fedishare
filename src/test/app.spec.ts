import sinon from 'sinon';
import { App } from '../lib/app';
import * as Dom from '../lib/dom';

describe('testing App', () => {
  let sandboxes;
  let renderItems;

  beforeEach(() => {
    sandboxes = sinon.createSandbox();
    renderItems = sandboxes.stub(Dom, 'renderItems');
  });

  afterEach(() => {
    sandboxes.restore();
  });

  describe('testing App', () => {
    it('app is rendered', () => {
      const app = new App();
      app.render();
      expect(renderItems.called).toBeTruthy();
    });
  });
});
